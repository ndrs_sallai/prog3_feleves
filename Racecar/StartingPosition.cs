﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Racecar
{
    class StartingPosition
    {
        public double X { get; set; }
        public double Y { get; set; }
        public int Angle { get; set; }
    }
}

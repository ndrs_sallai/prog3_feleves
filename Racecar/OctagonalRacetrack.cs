﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Racecar
{
    class OctagonalRacetrack : Racetrack
    {
        public OctagonalRacetrack(double centerX, double centerY) {
            Geometry combined = null;

            int numberOfRectangles = 8;
            int sectionWidth = 100;
            int sectionHeight = 50;

            for (int i = 0; i < numberOfRectangles; i++) {
                Geometry other = new RectangleGeometry(new Rect(centerX - sectionWidth / 2, centerY - 120, sectionWidth, sectionHeight));

                other.Transform = new RotateTransform(360 / numberOfRectangles * (i + 1), centerX, centerY);

                if (combined != null) { 
                    combined = Geometry.Combine(combined, other, GeometryCombineMode.Union, null);
                } else {
                    combined = other;
                }
            }

            Body = combined;
        }

        public override StartingPosition GetStartingPosition() {
            return new StartingPosition() {
                X = 50,
                Y = 50,
                Angle = 180,
            };
        }
    }
}

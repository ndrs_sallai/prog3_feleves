﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Racecar
{
    class GameObject
    {
        public Geometry Body { get; protected set; }

        protected void TransformGeometry(Transform transform) {
            // clone and set transformation
            Geometry clone = Body.Clone();
            clone.Transform = transform;

            // apply transformation
            Geometry transformed = clone.GetFlattenedPathGeometry();

            // replace body with transformed geometry
            Body = transformed;
        }

        public bool CollidesWith(GameObject other) {
            return Geometry.Combine(this.Body, other.Body, GeometryCombineMode.Intersect, null).GetArea() > 0;
        }

        public bool SticksOutOf(GameObject other) {
            return Geometry.Combine(this.Body, other.Body, GeometryCombineMode.Exclude, null).GetArea() > 0;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Racecar
{
    class Game
    {
        public Car Car { get; private set; }

        public Game(Car car) {
            this.Car = car;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace Racecar
{
    class GameFrameworkElement : FrameworkElement
    {
        protected Game game;
        protected Car car;
        protected Racetrack raceTrack;

        DispatcherTimer timer = new DispatcherTimer();

        public GameFrameworkElement() {
            this.Loaded += GameFrameworkElement_Loaded;
            this.KeyDown += GameFrameworkElement_KeyDown;
        }

        void GameFrameworkElement_KeyDown(object sender, System.Windows.Input.KeyEventArgs e) {
            if (e.Key == System.Windows.Input.Key.Left) {
                car.Rotate(-5);
            }
            
            if (e.Key == System.Windows.Input.Key.Right) {
                car.Rotate(5);
            }

            if (e.Key == System.Windows.Input.Key.Up) {
                car.Accelerate();
            }

            if (e.Key == System.Windows.Input.Key.Down) {
                car.Brake();
            }
        }

        void GameFrameworkElement_Loaded(object sender, RoutedEventArgs e) {
            //raceTrack = new OctagonalRacetrack((int)this.ActualWidth / 2, (int)this.ActualHeight / 2);
            raceTrack = new PathBasedRacetrack(this.ActualWidth / 2, this.ActualHeight / 2);

            StartingPosition pos = raceTrack.GetStartingPosition();

            car = new Car(pos.X, pos.Y, pos.Angle);

            this.game = new Game(car);

            this.InvalidateVisual();

            // make it focusable
            this.Focusable = true;

            // focus, so user events can be handled
            this.Focus();

            timer.Interval = new TimeSpan(0, 0, 0, 0, 20);
            timer.Tick += timer_Tick;
            timer.Start();
        }

        void timer_Tick(object sender, EventArgs e) {
            car.UpdatePosition();

            if (car.SticksOutOf(raceTrack)) {
                car.Stop();
            }

            // trigger re-draw
            this.InvalidateVisual();
        }

        protected override void OnRender(DrawingContext drawingContext) {
            base.OnRender(drawingContext);

            if (game != null) {
                drawingContext.DrawGeometry(Brushes.Pink, null, raceTrack.Body);
                drawingContext.DrawGeometry(Brushes.Black, null, car.Body);
                //drawingContext.DrawEllipse(Brushes.Orange,new Pen(Brushes.Teal, 5), new Point(car.CenterX, car.CenterY), 5, 5);
            }
        }
    }
}

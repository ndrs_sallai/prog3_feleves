﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Racecar
{
    class Car : GameObject
    {
        private int width = 20;
        private int height = 30;

        private int speed = 0;

        // initial angle (this is how we decide to draw the car: nose facing up)
        private int angle = 270;

        public double CenterX {
            get {
                return (this.Body.Bounds.Left + this.Body.Bounds.Right) / 2;
            }
        }

        public double CenterY {
            get {
                return (this.Body.Bounds.Top + this.Body.Bounds.Bottom) / 2;
            }
        }

        //protected List<BodyElement> bodyElements;

        public Car(double x, double y, int angle) {
            Geometry bodyBase = new RectangleGeometry(new Rect(x - width / 2, y - height / 2, this.width, this.height));

            /*Geometry frontLights = new RectangleGeometry(new Rect(x - width / 2, y - height / 2, this.width, 5));
            Geometry rearLights = new RectangleGeometry(new Rect(x - width / 2, y + height / 2 - 5, this.width, 5));

            this.bodyElements = new List<BodyElement>();
            this.bodyElements.Add(new BodyElement() { Geometry = bodyBase, Color = Brushes.Black });
            this.bodyElements.Add(new BodyElement() { Geometry = frontLights, Color = Brushes.Yellow });
            this.bodyElements.Add(new BodyElement() { Geometry = rearLights, Color = Brushes.Red });*/

            Body = bodyBase;

            // init rotation
            this.Rotate(angle);
        }

        /*public void Render(DrawingContext ctx) {
            foreach (BodyElement e in this.bodyElements) {
                ctx.DrawGeometry(e.Color, null, (e.Geometry));
            }
        }*/

        public void Accelerate() {
            this.speed += 1;
        }

        public void Brake() {
            this.speed -= 1;
        }

        public void Stop() {
            this.speed = 0;
        }

        public void UpdatePosition() {
            double dx = this.speed * (Math.Cos(this.angle * Math.PI / 180));
            double dy = this.speed * (Math.Sin(this.angle * Math.PI / 180));

            this.TransformGeometry(new TranslateTransform(dx, dy));
        }

        public void Rotate(int degrees) {
            this.angle += degrees;

            this.TransformGeometry(new RotateTransform(degrees, this.CenterX, this.CenterY));
        }
    }

    /*class BodyElement {
        public Geometry Geometry { get; set; }
        public Brush Color { get; set; }
    }*/
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Racecar
{
    abstract class Racetrack : GameObject
    {
        abstract public StartingPosition GetStartingPosition();
    }
}
